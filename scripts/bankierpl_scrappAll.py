#!/usr/bin/python3

import pandas as pd
import requests
from bs4 import BeautifulSoup
from repustate import Client
from google.cloud import bigquery


companies_list = ["gnb", "urs", "bra", "gcn", "kgh"]
companies_list_bankier = {"gnb":"GETINOBLE", "urs":"URSUS", "bra":"BRASTER", "gcn":"GROCLIN", "kgh":"KGHM"}
bankier_url = "https://www.bankier.pl"

client = Client(api_key='f8a3ef2ca131accd6834b2bf29034729368cd136', version='v4') #this api in the free trial will handle only up to 1000 requests


def getNumberOfArticlePages(url):
    get_resp = requests.get(url)
    news_page = BeautifulSoup(get_resp.text, 'html.parser')
    numerals_div = news_page.find(class_="numerals")
    number_of_pages = int(numerals_div.find_all("a")[-1].text)
    return number_of_pages

def getArticleText(article_page):
    article_content = article_page.find(id="articleContent")
    article_blocs = article_content.find_all("p")
    
    article_text = ""
    for i in range(0,3):    
        if i < len(article_blocs):
            article_text += article_blocs[i].text + " "
    
    return article_text

def getArticlesDf(url, max_articles):
    
    articles_counter = 0
    df = pd.DataFrame(columns=['title', 'timestamp', 'text'])
    
    number_of_pages = getNumberOfArticlePages(url)
        
    for i in range(1,number_of_pages+1):
        page_url = url +"/"+ str(i)
        page_responce = requests.get(page_url)
        page_soup = BeautifulSoup(page_responce.text, 'html.parser')
        articles_list = page_soup.find_all(class_="article")


        if articles_counter >= max_articles:
            break
            
        for article_entry in articles_list:
            article_url = bankier_url + article_entry.find(class_="more-link right")["href"]

            r_article_page = requests.get(article_url)
            r_article_page.encoding = 'utf-8'
            article_page_text = r_article_page.text
            article_page = BeautifulSoup(article_page_text, 'html.parser')

            article_data = {}

            try:
                article_data["title"] = article_page.find(class_="entry-title").text
                article_data["timestamp"] = pd.to_datetime(article_page.find(class_="entry-date")['datetime'])
                article_data["text"] = getArticleText(article_page)
                
            except AttributeError:
                #todo: handle this case properly, probably some inconsistency in webpagelayout
                break;
                
            df = df.append(article_data, ignore_index=True)
                   
            articles_counter += 1
            
            if articles_counter >= max_articles:
                break
            

    return df



# scrap articles
df_common = pd.DataFrame(columns=["title","timestamp","text", "company"])

for i in companies_list_bankier.items():
    company_name = i[1]
    url = "https://www.bankier.pl/gielda/notowania/akcje/"+company_name+"/wiadomosci"
    df = getArticlesDf(url, 100)
    df["company"] = i[0]
    df_common = df_common.merge(df, 'outer')


# calculate sentiment

def calculate_Sentimet(text):
    if (text != None) and (len(text) > 0):
        try:
            result = client.sentiment(text, lang="pl")
            return result["score"]
        except RepustateAPIError:
            # todo: handle this properly
            return 0;
    else:
        return 0;

df_common['sentiment'] = df_common.apply(lambda row: calculate_Sentimet(row.text), axis=1)

# push to BigQuery

df_common.to_gbq(destination_table="bankierpl.all_score", project_id="total-treat-228709")