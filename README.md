
# "Dead cat bounce" detection on Warsaw Stock Exchange 

Prototype for recruitment purposes.

Warsaw, Jerzy Balcerzak, 18.01.2018

-----

## Motivation

Dead cat bounce (DCB) is a pattern observed in the stock price - brief resurgence during or following a severe decline. [See wiki](https://en.wikipedia.org/wiki/Dead_cat_bounce)

Examples of the effect observed recently on GPW:  
[GNB (0.00%) - Getin Noble Bank SA - Stooq](https://stooq.pl/q/?s=gnb&c=1y&t=l&a=lg&b=0)   
[URS (+0.79%) - Ursus SA - Stooq](https://stooq.pl/q/?s=urs&c=1y&t=l&a=lg&b=0)   
[BRA (+0.56%) - Braster SA - Stooq](https://stooq.pl/q/?s=bra&c=1y&t=l&a=lg&b=0)

Being aware of upcoming DCB effect, one could use it in investment strategy for earning on 30-50% debounce effect.

#### Hypothesis:

Before Dead Cat Bounce Effect appears some kind of pattern (symptom) - that could be detected in stock parameters (price, volume), but also in the sentiment of the news regarding the company.

#### Project objective:

Aim of the project is to verify the above hypothesis on the small sample of known examples of Dead Cat Bounce effect by plotting price, volume of the stock and sentiment of the related articles.

Code is not production ready, rather a fast prototype to verify hypothesis and show usages of required technology components on the way.  

#### Technology

In order to fulfil technology requirements (ETL in python, usage of BigQuery, API integration, ML), project consists of following components:

1. Stock data upload to Google BigQuery 
2. Web scrapper exaction of the text of articles about company from web portal ([example for GNB](https://www.bankier.pl/gielda/notowania/akcje/GETINOBLE/wiadomosci)) 
3. Sentiment analysis - free version of SaaS service is used - [Repustate](https://www.repustate.com/sentiment-analysis-api-demo/), because building a sentiment analysis ML model would be a not trivial task (especially for polish language) and would outgrow one-day MVP project
4. Pushing articles text and sentiment index to BigQuery
5. Visualisation of the data collected in the BigQuery

----

## How to use / review

It is strongly recommended to review the code as [jupyther notebooks](https://www.quora.com/What-exactly-is-%E2%80%98Jupyter-Notebook%E2%80%99). 
Notebooks are made to be informative - they present the intermediate results,notes, and comments to the results.

Jupyther installation:
https://jupyter.readthedocs.io/en/latest/install.html

To run the notebooks, in the main directory of repository, run jupyther:
```bash
jupyther notebook
```
Then select the notebook to review.

### Notebooks  

1. import_stock_to_bq.ipynb - acquiring stock data and placing those in BQ 
2. bankierpl_scrapper.ipynb - an intermediate step, left for reviewing purposes. MVP of bankier.pl web scrapper and sentiment calculation. Take a look to see how scrapper was constructed.
3. bankierpl_scrappAll.ipynb - scrapping last 100 articles per company and placing those in BigQuery
4. plotting.ipynb - loading data from BigQuery and generating some nice interactive visualisations in plotly

### Srcipts
Although jupyther notebooks are recommended for a review, crucial pieces of code were placed in /scripts folder. Because of time constraints, those are poorly commented and would need firm refactoring.
 
##### why scripts won't run out of the box?
- dependencies (python packages) needs to be installed
- [BigQuery authentication](https://cloud.google.com/bigquery/docs/authentication/) needs to be set up
- Repustate authentication needs to be set up

For more details contact the author jerzybalcerzak@gmail.com

----

## Results discussion & possible next steps 


The first conclusion is that sentiment scoring does not meet the intuition. Its domain is (-1,1) with -1 "negative" and +1 positive sentiment, but acquired samples tend to be close to edge values or 0. There is no obvious pattern in articles sentiment visible that would related to the dead cat bounce effect for most of the cases.

Examples:

![](results/gnb.png "GNB")
![](results/urs.png "URS")


The Groclin (GCN) company results seem to be an exception. In this case sentiment of articles seem to be strongly correlated to price of the stock (what is actually very intuitive), but also has a rising trend in the "bouncing" stage.

![](results/gcn.png "GCN")


Issues and possible next steps:

- The results on such a small group of DCB examples can be misleading. Possibly automatic search for more Dead Can Bounce cases in the stock exchange history would be a way to gather more data. That could be done by f.e. fitting straight line to the price of the stock and looking for a negative slope and high +40% change during 12 months window.

- The quality of sentiment analysis is unknown. In this project "repustate" service was used in a black box manner. Probably, the proper way would be to gather a dataset of articles and label it (manually), then train a custom model on that. There is a lot of new achievements in the domain, but NLP for polish language is not very common topic. Maybe text translation using google translate, then analysing sentiment of text in english would be a "low hanging fruit" development path. 

- In case of achieving better dataset and sentiment analysis solution in the future, I would propose using a sequence search algorithms to look for a Bead Cat Bounce effect symptoms in the data.